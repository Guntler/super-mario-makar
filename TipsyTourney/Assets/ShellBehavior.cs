﻿using UnityEngine;
using System.Collections;

public class ShellBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(string.Format("{0}", GetComponent<Rigidbody>().velocity.magnitude));
        if(GetComponent<Rigidbody>().velocity.magnitude > 0)
        {
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(0, 0, GetComponent<Rigidbody>().velocity.magnitude));
        }
    }
}
