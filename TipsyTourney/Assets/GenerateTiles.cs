﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Tuple<T1, T2>
{
    public T1 First { get; private set; }
    public T2 Second { get; set; }
    internal Tuple(T1 first, T2 second)
    {
        First = first;
        Second = second;
    }
}

public static class Tuple
{
    public static Tuple<T1, T2> New<T1, T2>(T1 first, T2 second)
    {
        var tuple = new Tuple<T1, T2>(first, second);
        return tuple;
    }
}

public class GenerateTiles : MonoBehaviour {

    List<Tuple<GameObject,bool>> tiles = new List<Tuple<GameObject, bool>>();
    int width = 7;
    int height = 17;
    int totalTiles = 0;
    int consumedTiles = 0;

    // Use this for initialization
    void Start () {
        totalTiles = width * height;
        var otile = GameObject.FindWithTag("tile");
        
        for(int r=0;r<7;r++)
        {
            for(int c=0;c<17;c++)
            {
                var tile = Instantiate(otile);
                tile.transform.parent = this.transform;
                tile.transform.position = otile.transform.position;
                var cx = tile.GetComponent<Renderer>().bounds.size.x;
                var cz = tile.GetComponent<Renderer>().bounds.size.z;
                tile.transform.position += new Vector3(cx * c * 3, 0,  r * cz * 3);
                tile.transform.localScale = otile.transform.localScale;
                var name = "tile_";
                tile.name = name + r.ToString() /*+ "_" + c.ToString()*/;
                tiles.Add(new Tuple<GameObject,bool>(tile,false));
            }
        }
        Destroy(otile);
        //if(tile==null) Debug.Log(string.Format("HELLO"));
        //tile.transform.position += new Vector3(-8, 0, -3);
        /*var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube.name = "tile";
        cube.transform.localScale.Scale(new Vector3(1,(float) 0.01, 1));
        
        cube.transform.position = this.transform.position+ new Vector3(-8,0,-3);

        Instantiate(cube);*/
        
        
    }
	
	// Update is called once per frame
	void Update () {
        if (consumedTiles>=totalTiles)
        {
            Debug.Log(string.Format("VICTORY!"));
        }
        else
        {
            //Debug.Log(string.Format("{0}",consumedTiles));
        }
	}
    
    public void RemoveTile(int x, int y)
    {
        tiles[x].Second = true;
        consumedTiles++;
    }
}
