﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        float x = GetComponent<Rigidbody>().rotation.x;
        float y = GetComponent<Rigidbody>().rotation.y;
        float z = GetComponent<Rigidbody>().rotation.z;
        //float sx = GameObject.FindWithTag("shell").GetComponent<Rigidbody>().rotation.x;
        float sz = GameObject.FindWithTag("shell").GetComponent<Rigidbody>().rotation.z;
        if (Input.GetKey("w"))
        {
            GameObject.FindWithTag("shell").GetComponent<Rigidbody>().transform.Rotate(new Vector3(0, 0, sz - 1));
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(0, 0, z-1));
        }
        if (Input.GetKey("s"))
        {
            //GameObject.FindWithTag("shell").GetComponent<Rigidbody>().transform.Rotate(new Vector3(0, 0, sz + 1));
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(x, y, z + 1));
        }
        if (Input.GetKey("a"))
        {
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(x - 1, y,z));
        }
        if (Input.GetKey("d"))
        {
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(x + 1, y, z));
        }
        if (Input.GetKey("q"))
        {
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(x, y-1, z));
        }
        if (Input.GetKey("e"))
        {
            GetComponent<Rigidbody>().transform.Rotate(new Vector3(x, y+1, z));
        }
    }
}
