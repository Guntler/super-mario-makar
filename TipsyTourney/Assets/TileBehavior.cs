﻿using UnityEngine;
using System.Collections;
using System;

public class TileBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
    }

    void OnTriggerEnter( Collider other)
    {
        if (other.gameObject.CompareTag("shell"))
        {
            other.GetComponent<AudioSource>().Play();
            String[] strs = name.Split('_');
            GameObject.FindWithTag("board").GetComponent<GenerateTiles>().RemoveTile(Int32.Parse(strs[1]), 0);
            Destroy(this.gameObject);
        }
    }
}
