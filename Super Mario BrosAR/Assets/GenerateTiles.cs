﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using System.Linq;

public class Tuple<T1, T2>
{
    public T1 First { get; private set; }
    public T2 Second { get; set; }
    internal Tuple(T1 first, T2 second)
    {
        First = first;
        Second = second;
    }
}

public static class Tuple
{
    public static Tuple<T1, T2> New<T1, T2>(T1 first, T2 second)
    {
        var tuple = new Tuple<T1, T2>(first, second);
        return tuple;
    }
}

public class GenerateTiles : MonoBehaviour
{
    List<Tuple<GameObject,bool>> tiles = new List<Tuple<GameObject, bool>>();
    int width = 7;
    int height = 17;
    int totalTiles;
    int consumedTiles;

    public void StartGeneration()
    {
        totalTiles = width * height;
        var otile = GameObject.FindWithTag("tile");

        foreach (Transform child in transform)
        {
            if (child.tag == "shell")
            {
                child.gameObject.SetActive(true);
            }
        }

        System.Random rand = new System.Random();
        var randLock = new System.Object();
        var numericLock = new System.Object();
        var nBlocks = 0;
        var tileNo = 0;
        for (int r = 0; r < 7; r++)
        {
            var nBlockRow = 0;
            for (int c = 0; c < 17; c++)
            {
                var tile = Instantiate(otile);
                tile.transform.parent = this.transform;
                tile.transform.position = otile.transform.position;
                var cx = tile.GetComponent<Renderer>().bounds.size.x;
                var cz = tile.GetComponent<Renderer>().bounds.size.z;
                tile.transform.position += new Vector3(cx * c * 3, 0, r * cz * 3);
                tile.transform.localScale = otile.transform.localScale;
                var result = 0.0;
                var isBlock = false;
                lock (randLock)
                {
                    result = rand.Next(0,100) / (nBlockRow+5);
                    var result2 = rand.Next(0, 300);
                    if (result> result2 && nBlocks < width)
                    {
                        isBlock = true;
                    }
                }
                var name = "";
                if (isBlock)
                {

                    name = "block_";
                    nBlocks++;
                    nBlockRow++;
                }
                else
                    name = "tile_";
                tile.name = name + tileNo.ToString();
                tiles.Add(new Tuple<GameObject, bool>(tile, false));
                tileNo++;
            }
        }
        Destroy(otile);


    }

    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {
        if(!GameObject.FindWithTag("camera").GetComponent<GameLogic>().finished &&
                GameObject.FindWithTag("camera").GetComponent<GameLogic>().started)
        {
            if ((consumedTiles >= totalTiles && consumedTiles > 0)/* || consumedTiles>1*/)
            {
                Debug.Log("VICTORY");
                GameObject.FindWithTag("camera").GetComponent<GameLogic>().TriggerVictory();
            }
            else
            {
            }
        }
	}
    
    public void RemoveTile(int x, int y)
    {
        if (consumedTiles != 0 && totalTiles != 0)
            Debug.Log(string.Format("{0}/{1}", consumedTiles, totalTiles));
        Debug.Log(string.Format("Tile {0} added {1}", x, consumedTiles));
        if (!tiles[x].Second)
        {
            tiles[x].Second = true;
            consumedTiles++;
            Debug.Log(string.Format("Tile {0} added {1}", x, consumedTiles));
        }
    }
}
