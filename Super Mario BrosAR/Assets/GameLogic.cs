﻿using UnityEngine;
using System.Collections;

public class GameLogic : MonoBehaviour {

    public bool started = false;
    public bool finished = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void TriggerStart()
    {
        started = true;
    }

    public void TriggerVictory()
    {
        if(!finished)
        {
            GetComponent<AudioSource>().Play();
            foreach (Transform child in GameObject.FindWithTag("stagemarker").transform)
            {
                if (child.tag == "endgame")
                {
                    child.gameObject.SetActive(true);
                    for(int i=0;i<5;i++)
                    {
                        child.Translate(0, -2, 0);
                    }
                }
            }
            finished = true;
        }
    }
}
