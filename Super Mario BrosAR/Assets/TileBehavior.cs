﻿using UnityEngine;
using System.Collections;
using System;

public class TileBehavior : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
    }

    void OnTriggerEnter( Collider other)
    {
        if(GameObject.FindWithTag("camera").GetComponent<GameLogic>().started)
        {
            if (!gameObject.CompareTag("titletextbox") && other.gameObject.CompareTag("shell"))
            {

                String[] strs = name.Split('_');
                GameObject.FindWithTag("board").GetComponent<GenerateTiles>().RemoveTile(Int32.Parse(strs[1]), 0);
                if (strs[0].Equals("block"))
                {
                    GetComponent<AudioSource>().Play();
                    var tex = Resources.Load("tile_brick_empty");
                    GetComponent<Renderer>().material.mainTexture = (Texture)tex;
                    other.GetComponent<Rigidbody>().velocity *= -1;
                }
                else
                {
                    other.GetComponent<AudioSource>().Play();
                    Destroy(this.gameObject);
                }
            }
        }
        else
        {
            if(gameObject.CompareTag("titletextbox") && other.gameObject.CompareTag("shell"))
            {
                var clip = Resources.Load("smb3_hurry_up");
                other.GetComponent<AudioSource>().PlayOneShot((AudioClip)clip);
                GameObject.FindWithTag("titletext").SetActive(false);
                gameObject.SetActive(false);
                GameObject.FindWithTag("camera").GetComponent<GameLogic>().TriggerStart();
            }
        }
    }
}
