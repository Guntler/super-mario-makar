/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Qualcomm Connected Experiences, Inc.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES
        private bool generatedOnce = false;
        private TrackableBehaviour mTrackableBehaviour;
    
        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS
    
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
            if(!generatedOnce)
            {
                generatedOnce = true;
                GameObject.FindWithTag("board").GetComponent<GenerateTiles>().StartGeneration();
                GameObject.FindWithTag("camera").GetComponent<VuforiaBehaviour>().SetWorldCenterMode(VuforiaAbstractBehaviour.WorldCenterMode.CAMERA);
            }
            else
            {
                
            }
            foreach (Transform child in transform)
            {
                if (child.tag == "board")
                {
                    foreach (Transform child1 in child.transform)
                    {
                        if (child1.tag == "shell")
                        {
                            child1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
                            child1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
                            var postxt = GameObject.FindWithTag("postxt").GetComponent<Text>();
                            if (postxt)
                            {
                                postxt.text = "";
                            }
                            /*var postxt = GameObject.FindWithTag("coltxt").GetComponent<Text>();
                            postxt.text = "FOUND SHELL";*/
                        }
                    }
                }
            }
        }


        private void OnTrackingLost()
        {
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            foreach (Transform child in transform)
            {
                if (child.tag == "board")
                {
                    foreach (Transform child1 in child.transform)
                    {
                        if (child1.tag == "shell")
                        {
                            child1.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition;
                            /*var postxt = GameObject.FindWithTag("coltxt").GetComponent<Text>();
                            postxt.text = "LOST SHELL";*/
                            var postxt = GameObject.FindWithTag("postxt").GetComponent<Text>();
                            if (postxt)
                            {
                                postxt.text = "MARKER LOST";
                            }
                        }
                    }
                }
            }
            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
        }

        #endregion // PRIVATE_METHODS
    }
}
